---
title: Kubernetes
description: 
published: true
date: 2021-12-24T03:25:52.704Z
tags: 
editor: markdown
dateCreated: 2021-11-28T02:05:30.263Z
---

# Kubernetes

## Credentials
Credentials are managed through GitLab with [External Secrets](https://github.com/external-secrets/external-secrets).

`external-secrets` needs a GitLab API token with the `api` scope for the application's repository to access its variables. If `gitlab-api-token` does not currently exist in the application's namespace, create it using the following:
```bash
export NAMESPACE=
export GITLAB_API_TOKEN=

kubectl -n $NAMESPACE create secret generic gitlab-api-token --type='Opaque' --from-literal=token=$GITLAB_API_TOKEN --dry-run=client -o yaml \
| kubectl label -f - --dry-run=client -o yaml --local type=gitlab \
| kubectl apply -f -
```

## Image Pull Secret
Whether deployed with ArgoCD or manually with `kustomize` or `helm`, deployments using images from private repositories will not succeed unless a valid `imagePullSecret` is available in the cluster namespace. There are two ways to get the necessary credentials:
1. For a specific GitLab project, generate an access token with `read_registry` scope
1. For an entire GitLab group, generate a deploy token with `read_registry` scope
	- Note: If you specify a username, you **must** use that username. Otherwise, anything is accepted in the `--docker-username` parameter.
  
Then, create the Secret:
```bash
export NAMESPACE=
export SECRET_NAME=gitlab-registry
export TOKEN_USERNAME=
export TOKEN=
export EMAIL=
# either "deploy" for Group or "access" for Project
export TOKEN_TYPE=
# either the Group name or Project name
export TOKEN_CONTEXT=

kubectl -n $NAMESPACE create secret docker-registry $SECRET_NAME --docker-server=https://gitlab.com --docker-username=$TOKEN_USERNAME --docker-password=$TOKEN 00docker-email=$EMAIL --dry-run=client -o yaml  \
| kubectl label -f - --dry-run=client -o yaml --local gitlab.com/token-type=$TOKEN_TYPE --local gitlab.com/token-context=$TOKEN_CONTEXT \
| kubectl apply -f -
```

Unless the access/deploy token has an associated username, any username will be accepted by the `--docker-username` parameter. Likewise with `--docker-email` as this parameter is not used.

If the credentials you need already exist in another namespace, see [Secret Duplication](/kubernetes#secret-duplication) for how to copy the credentials to your new namespace.

## Secret Duplication
If a secret you need already exists in another namespace, you can copy that secret into the new namespace with:
```bash
export SECRET_NAME=gitlab-registry
export EXISTING_SECRET_NAMESPACE=
export NEW_SECRET_NAMESPACE=

kubectl -n $EXISTING_SECRET_NAMESPACE get secret $SECRET_NAME -o json \
| jq 'del(.metadata.namespace,.metadata.resourceVersion,.metadata.uid)' \
| kubectl -n $NEW_SECRET_NAMESPACE apply -f -
```