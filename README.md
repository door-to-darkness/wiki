---
title: README
description: 
published: false
date: 2021-11-23T16:53:37.942Z
tags: 
editor: markdown
dateCreated: 2021-11-23T16:52:15.959Z
---

# Wiki
This repository is the Git storage module for a [Wiki.js](https://js.wiki/) wiki.

[You can view the wiki here](https://wiki.tramonte.us) or bring it up locally using `docker-compose`/`podman-compose` and configuring the Git storage module to point to this repository.
